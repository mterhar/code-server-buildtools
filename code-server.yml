apiVersion: v1
kind: Namespace
metadata:
  name: development
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: developer
  namespace: development
---
apiVersion: v1
kind: Secret
metadata:
  name: developer-secret
  annotations:
    kubernetes.io/service-account.name: developer
type: kubernetes.io/service-account-token
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: development
  name: developer
rules:
- apiGroups: ["", "extensions", "apps"]
  resources: ["*"]
  verbs: ["*"]
- apiGroups: ["batch"]
  resources:
  - jobs
  - cronjobs
  verbs: ["*"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: dev-in-dev
  namespace: development
subjects:
- kind: ServiceAccount
  name: developer
  namespace: development
roleRef:
  kind: Role
  name: developer
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: code-server
  namespace: development
  annotations:
    kubernetes.io/ingress.class: ingress-public
    cert-manager.io/cluster-issuer: "letsencrypt"
    nginx.ingress.kubernetes.io/proxy-body-size: 500m
    nginx.ingress.kubernetes.io/client-max-body-size: 500m
    nginx.ingress.kubernetes.io/proxy-connect-timeout: 75s
    nginx.ingress.kubernetes.io/proxy-read-timeout: 60s
spec:
  tls:
  - hosts:
    - code.external.com
    secretName: codeserver-prod
  rules:
  - host: code.external.com
    http:
      paths:
      - backend:
          serviceName: code-server
          servicePort: 80
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: code-server-hugo
  namespace: development
  annotations:
    kubernetes.io/ingress.class: media
    nginx.ingress.kubernetes.io/configuration-snippet: |
      proxy_set_header Connection "upgrade";
      proxy_set_header Upgrade "websocket";
spec:
  rules:
  - host: hugo.internal.com
    http:
      paths:
      - backend:
          serviceName: code-server-hugo
          servicePort: 80
---
apiVersion: v1
kind: Service
metadata:
 name: code-server-hugo
 namespace: code-server
spec:
 ports:
 - port: 80
   targetPort: 1313
 selector:
   app: code-server
---
apiVersion: v1
kind: Service
metadata:
 name: code-server
 namespace: code-server
spec:
 ports:
 - port: 80
   targetPort: 8080
 selector:
   app: code-server
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  annotations:
    volume.beta.kubernetes.io/storage-class: openebs-sc-statefulset
    volume.beta.kubernetes.io/storage-provisioner: openebs.io/provisioner-iscsi
  name: code-server-pvc
  namespace: default
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 10Gi
  storageClassName: openebs-sc-statefulset
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: code-server
  name: code-server
  namespace: default
spec:
  selector:
    matchLabels:
      app: code-server
  replicas: 1
  template:
    metadata:
      labels:
        app: code-server
    spec:
      imagePullSecrets:
        - name: gitlabmjt
      containers:
      - image: registry.gitlab.com/mterhar/code-server-buildtools:v1.2
        imagePullPolicy: Always
        name: code-server
        env:
        - name: PASSWORD
          value: "PICKANICEPASSWORDFORTHIS"
        volumeMounts:
        - mountPath: /home
          name: code-server-vol
        livenessProbe:
          httpGet:
            path: /login
            port: 8080
          timeoutSeconds: 120
        readinessProbe:
          httpGet:
            path: /login
            port: 8080
          timeoutSeconds: 120
        resources:
          limits:
            cpu: 3000m
            memory: 4000Mi
          requests:
            cpu: 200m
            memory: 724Mi
      securityContext:
        runAsUser: 1000
        runAsGroup: 1000
        fsGroup: 1000
      volumes:
      - name: code-server-vol
        persistentVolumeClaim:
          claimName: code-server-pvc
          readOnly: false
